<!-- footer -->
<footer class="main-footer">
    <div class="columns">
      <div class="column is-6">
        <h1 class="name">Nous joindre</h1>
        <figure class="image is-128x128">
          <img class="is-rounded" src="<?php echo get_template_directory_uri(); ?>/img/trebek.jpg">
        </figure>
        <h2 class="subtitle is-4">Mark Ladate</h2>
        <ul class="boast"> 
          <ol>Auteur / Conférencier</ol> 
          <ol>Specialiste en questions inversées.</ol>
          <ol>Superbe moustache</ol>
          <ol><a href="#">mark@ladate.com</a></ol>
        </ul>
      </div>
        <div class="column is-2 is-offset-3">
          <h1 class="subtitle is-4 name">K-10 productions </h1>
          
            123 avenue de Kitch
            <br>Québec, Qc
            <br>G4G H4H
            <br>Téléphone: <a href="#">666-999-7777</a>
            <br>Courriel: <a href="#">info@ktaine.com</a>
         
        </div>
  </footer>
</body>
<?php wp_footer(); ?>
</html>