<?php get_header(); ?>

<body>
  <section class="hero is-medium">
    <div class="hero-body">
      <div class="container">
        <div class="columns">
          <div class="column is-12-desktop ">
            <h1 class="title is-2 is-spaced">
              N'oublie pas
            </h1>
            <h2 class="subtitle is-4">
              Le plus grand événement littéraire depuis les dames de coeur
            </h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section">
    <div class="columns is-centered">
      <div class="column is-one-quarter">
        <div class="card">
          <div class="card-image">
            <figure class="image is-4by3">
              <img src="img/treb.jpg" alt="Placeholder image">
            </figure>
          </div>
          <div class="card-content">
            <div class="media">
              <div class="media-left">
                <figure class="image is-48x48">
                  <img src="img/treb.jpg" alt="Placeholder image">
                </figure>
              </div>
              <div class="media-content">
                <p class="title is-4">Mark Ladate</p>
                <p class="subtitle is-6">Auteur de la trilogie</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="profil column is-one-quarter">
        <p class="title is-4">Le profil de l'auteur</p>
        <p>Considérant la difficulté de ces derniers temps, nous sommes contraints d'inventorier la somme des voies de bon sens, parce qu'il est temps d'agir.</p>  
        <p>Avec la politique de l'époque actuelle, nous sommes contraints de caractériser la globalité des ouvertures de bon sens, avec beaucoup de recul.</p>            
        <p>Compte tenu de complexité actuelle, il faut favoriser la simultanéité des options emblématiques, parce que les mêmes causes produisent les mêmes effets.</p>            
        <a href="#">#moustache</a> <a href="#">#sexy</a>  
      </div>

    </div>
  </section>
  <section class="books">
    <div class="columns is-multiline is-mobile">
      <div class="column">
        <section class="t-1">
          <header>
            <h2 class="title is-4">Noublie pas - <em>La date du premier rendez-vous</em></h2>
            <h3 class="title is-3">19 septembre 1968</h1>
          </header>
          <article>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam quidem nihil alias accusamus
            repellendus! Explicabo, illum earum. Quisquam recusandae omnis quis pariatur molestiae, praesentium facere
            vel velit saepe aliquam aperiam.</article>
          <footer><button class="button is-info ">Acheter</button></footer>
        </section>
      </div>
      <div class="column">
        <section class="t-2">
          <header>
            <h2 class="title is-4">Noublie pas - <em>La date de son anniversaire</em></h2>
            <h3 class="title is-3">22 juillet 1948</h1>
          </header>
          <article>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam quidem nihil alias accusamus
            repellendus! Explicabo, illum earum. Quisquam recusandae omnis quis pariatur molestiae, praesentium facere
            vel velit saepe aliquam aperiam.</article>
          <footer><button class="button is-info ">Acheter</button></footer>
        </section>
      </div>
      <div class="column">
        <section class="t-3">
          <header>
            <h2 class="title is-4">Noublie pas - <em>La date du mariage</em></h2>
            <h3 class="title is-3">19 juin 1970</h1>
          </header>
          <article>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam quidem nihil alias accusamus
            repellendus! Explicabo, illum earum. Quisquam recusandae omnis quis pariatur molestiae, praesentium facere
            vel velit saepe aliquam aperiam.</article>
          <footer><button class="button is-info ">Acheter</button></footer>
        </section>
      </div>
    </div>
  </section>

  <!-- Section  -->
  <section class="container crits">
    <header class="m-title">
      <h1 class="title is-2 is-spaced mgb-large">Ce que disent les critiques</h1>
    </header>
    <div class="tile is-ancestor">
      <article class="tile">
        <div class="tile is-parent">
          <article class="tile is-child notification is-success">
            <p class="subtitle">Cette trilogie est le chef-d'oeuvre de toutes mes soiree a la prison.</p>
            <p class="name">Ulrich Massicotte - Gardien de prison</p>
          </article>
        </div>
        <div class="tile is-parent">
          <article class="tile is-child notification is-primary">
            <p class="subtitle">J'aime beaucoup le style de Monsieur Ladate, c'est brun et 1970 a souhait.</p>
            <p class="name">Farnande Remington - Retraitée</p>
          </article>
        </div>
        <div class="tile is-parent">
          <article class="tile is-child notification is-danger">
            <p class="subtitle">UN excellent moyen de m'endormir rapidement pour éviter les demandes de mon méri</p>
            <p class="name">Natrelle Lactée - Fermière</p>
          </article>
        </div>
        <div class="tile is-parent">
          <article class="tile is-child notification is-success">
            <p class="subtitle">Wop diwop didlidlida !</p>
            <p class="name">Verlaine Casgrain - Illuminatrice d'idée</p>
          </article>
        </div>
      </article>
    </div>
  </section>
<?php 

get_footer(); 

?>
