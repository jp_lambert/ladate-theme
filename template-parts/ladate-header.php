<section class="hero is-medium">
    <div class="hero-body">
      <div class="container">
        <div class="columns">
          <div class="column is-12-desktop ">
            <h1 class="title is-2 is-spaced">
              N'oublie pas
            </h1>
            <h2 class="subtitle is-4">
              Le plus grand événement littéraire depuis les dames de coeur
            </h2>
          </div>
        </div>
      </div>
    </div>
  </section>