<?php
    get_header();

    get_template_part("template-parts/ladate", "newsletter");
    $titre = get_the_title();
    echo "<h1 style='font-size:28px;'>".$titre."</h1>";
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post(); 
            
            the_content();
        } // end while
    } // end if
    get_footer();