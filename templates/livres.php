<?php
/**
* Template Name: Test template
*/
    get_header();
    the_title();
    $args = array(
        'post_type'      => 'livres',
        'posts_per_page' => 10,
    );
    $loop = new WP_Query($args);

    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() ) {
            $loop->the_post(); 
        } // end while
    } // end if
    get_footer();