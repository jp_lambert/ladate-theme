<?php
/**
* Template Name: Auteur
*/
    get_header();
    the_title();
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post(); 
            get_template_part("template-parts/ladate", "profile");
        } // end while
    } // end if
    get_footer();