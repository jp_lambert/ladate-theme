<?php

add_action( 'wp_enqueue_scripts', 'enqueue_styles_ladate' );
function enqueue_styles_ladate() {
    wp_enqueue_style( 'style-ladate', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'style-principal', get_template_directory_uri().'/css/main.css' );
}

add_action("init", "create_book", 10, 3);

function create_book(){
    register_post_type('livres',
        array(
            'labels'      => array(
                'name'          => __('Livres', 'textdomain'),
                'singular_name' => __('Livre', 'textdomain'),
            ),
                'public'      => true,
                'has_archive' => false,
                'supports'    => [
                    'excerpt','title','editor'
                ]
        )
    );
}