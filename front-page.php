<?php
    get_header(); 
    get_template_part("template-parts/ladate", "header");
    get_template_part("template-parts/ladate", "profile");  
    get_template_part("template-parts/ladate", "books");  
    get_template_part("template-parts/ladate", "critiques");  
    get_template_part("template-parts/ladate", "newsletter");
    get_footer();